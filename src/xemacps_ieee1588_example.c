#include "xil_types.h"
#include "xil_io.h"
#include "xil_assert.h"
#include "xparameters.h"
#include "xgpio.h"
#include "xstatus.h"
#include "stdio.h"
#include "sleep.h"
#include "xparameters.h"
#include "xparameters_ps.h" /* defines XPAR values */
#include "xil_types.h"
#include "xil_assert.h"
#include "xil_io.h"
#include "xil_exception.h"
#include "xpseudo_asm.h"
#include "xil_cache.h"
#include "xil_printf.h"
#include "xscugic.h"
#include "xscutimer.h"
#include "xemacps.h" /* defines XEmacPs API */
#include "xil_mmu.h"
#include "xllfifo_interrupt.h"
#include "common_header.h"
#include "pl_comms_axi.h"
#include <inttypes.h>
#include "gpio_api.h"

#include "command.h"


#define CONTROL_GPIO_ID			XPAR_GPIO_0_DEVICE_ID
#define CONTROL_GPIO_CHANNEL	1

#define RESET_PIN 			    0x01   /* Assumes bit 0 of GPIO is connected to an Reset  */
#define GPIO_1_EXAMPLE_DEVICE_ID  XPAR_GPIO_1_DEVICE_ID
#define RESET_CHANNEL 			1




/************************** Constant Definitions *****************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters_ps.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */

/************ Internal Flag Defines*****************/
#define TX_MESSAGE_INTERNAL 0
#define TX_MESSAGE_PS 1
#define CURRUNT_TX_MESSAGE TX_MESSAGE_PS

#define EMACPS_DEVICE_ID XPAR_XEMACPS_0_DEVICE_ID
#define INTC_DEVICE_ID XPAR_SCUGIC_SINGLE_DEVICE_ID
#define TIMER_DEVICE_ID XPAR_SCUTIMER_DEVICE_ID
#define EMACPS_IRPT_INTR XPS_GEM0_INT_ID
#define TIMER_IRPT_INTR XPAR_SCUTIMER_INTR

#define RX_BD_START_ADDRESS 0x0FF00000
#define TX_BD_START_ADDRESS 0x0FF10000

/* Timer load value for timer expiry in every 500 milli seconds. */
#define TIMER_LOAD_VALUE XPAR_CPU_CORTEXA9_0_CPU_CLK_FREQ_HZ / (0x1 << TIMER_DIVIDER)

/**************************** Type Definitions *******************************/

/***************** Macros (Inline Functions) Definitions *********************/

#define RXBD_SPACE_BYTES \
	XEmacPs_BdRingMemCalc(XEMACPS_BD_ALIGNMENT, XEMACPS_IEEE1588_NO_OF_RX_DESCS)

#define TXBD_SPACE_BYTES \
	XEmacPs_BdRingMemCalc(XEMACPS_BD_ALIGNMENT, XEMACPS_IEEE1588_NO_OF_TX_DESCS)

/************************** Defination For FIFO *****************************/
#define FIFO_DEV_ID XPAR_AXI_FIFO_0_DEVICE_ID

#define INTC_DEVICE_ID XPAR_SCUGIC_SINGLE_DEVICE_ID
#define FIFO_INTR_ID XPAR_FABRIC_ZYNQ_PROC_DESIGN_DESIGN_1_I_ZYNQSS_AXI_FIFO_MM_S_0_INTERRUPT_INTR
//#define FIFO_INTR_ID XPAR_FABRIC_ZYNQSS_AXI_FIFO_MM_S_0_INTERRUPT_INTR

#define INTC XScuGic
#define INTC_HANDLER XScuGic_InterruptHandler

extern struct Packet PartialPacket;
/************************** Variable Definitions *****************************/

static XScuGic IntcInstance;	/* The instance of the SCUGic Driver */
static XScuTimer TimerInstance; /* The instance of the SCUTimer Driver */

XLlFifo FifoInstance;
XGpio Gpio; /* The Instance of the GPIO Driver */

/*
 * Instance of the Interrupt Controller
 */
u16 PacketCount = 0;

u32 TimeStamp = 0;
u32 ReceivePacket = 0;
u32 ForwardPacket = 0;
u16 UsedBDs = 0;
u8 EmacStatus = 0;
u16 PaketsPerSec = 0, FailedPacket = 0;
u16 MaxQueue = 0;
extern int ReceivedPacket;
extern int ReceivedFrame;
u16 ReceivedCnt, ReceivedCntTotal;
u16 TransmitCnt = 0, TransmittedCntTotal = 0, ErrCnt = 0;
u8 Tick, Print, TickCount;
u32 instanceCnt=0;

u16 TransferWord = 0;
u16 PacketCounter = 0;
u16 PrintCount=0;
u8 print_stop_flag = 0;

u32 ReceiveDataLength = 0, Occu;

u8 CompareByteData, CompareWordData;
u16 PrevChunkNum;

/************************** Function Prototypes ******************************/

int XEmacPs_InitScuTimer(void);
int XEmacPs_SetupIntrSystem(XScuGic *IntcInstancePtr,
							XScuTimer *TimerInstancePtr,
							XLlFifo *FifoInstancePtr,
							u16 TimerIntrId,
							u16 FifoIntrId);
void XEmacPs_RunIEEE1588Protocol();
void XEmacPs_TimerInterruptHandler();
int initialize_pl_modem();
void write_control_bytes(uint32_t *value, uint8_t);

int main(void)
{
	int Status = XST_SUCCESS;
	u32 Tspeed;

	xil_printf("SYSTEM_INFO:Entering into main() \r\n");
	xil_printf("SYSTEM_INFO:PACKET_LENGTH %d\r\n", PACKET_LENGTH);
	xil_printf("SYSTEM_INFO:TRANSFER_PACKETS %d\r\n", TRANSFER_PACKETS);
	xil_printf("SYSTEM_INFO:TIMER_DIVIDER %d\r\n", (0x1 << (TIMER_DIVIDER-1)));
	Tspeed = (PACKET_LENGTH) * 8 * TRANSFER_PACKETS * (0x01 << (TIMER_DIVIDER - 1));
	xil_printf("Speed %d bps, %d Kbps, %d Mbps\r\n", Tspeed, Tspeed / 1024, Tspeed / (1024 * 1000));

	Xil_SetTlbAttributes(0x0FF00000, 0xc02); // Addr, Attr

	//	***********************  Initialize SCUTIMER **************************
	if (XEmacPs_InitScuTimer() != XST_SUCCESS)
		while (1)
			;
	picozedsdr_init();

	//	*********************** Setting up FIFO Routine **************************
	SetupFIFO(&FifoInstance, FIFO_DEV_ID);
	/*
	 * Connect to the interrupt controller and enable interrupts in
	 * interrupt controller.
	 */
	Status = XEmacPs_SetupIntrSystem(&IntcInstance,
									 &TimerInstance, &FifoInstance,
									 TIMER_IRPT_INTR, FIFO_INTR_ID);
	if (Status != XST_SUCCESS)
	{
		return XST_FAILURE;
	}
	/*
	 * Enable the timer interrupt in the timer module
	 */
	XScuTimer_EnableInterrupt(&TimerInstance);
	/*
	 * Enable FIFO Interrupt in FIFO Module
	 */
	XLlFifo_IntEnable(&FifoInstance, XLLF_INT_ALL_MASK);
	xil_printf("SYSTEM_INFO:Running Application now \r\n");




	XEmacPs_RunIEEE1588Protocol();
	return 0;
}

int XEmacPs_InitScuTimer(void)
{
	int Status = XST_SUCCESS;
	XScuTimer_Config *ConfigPtr;

	/*
	 * Get the configuration of Timer hardware.
	 */
	ConfigPtr = XScuTimer_LookupConfig(TIMER_DEVICE_ID);

	/*
	 * Initialize ScuTimer hardware.
	 */
	Status = XScuTimer_CfgInitialize(&TimerInstance, ConfigPtr,
									 ConfigPtr->BaseAddr);
	if (Status != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	XScuTimer_EnableAutoReload(&TimerInstance);

	/*
	 * Initialize ScuTimer with a count so that the interrupt
	 * comes every 500 msec.
	 */
	XScuTimer_LoadTimer(&TimerInstance, TIMER_LOAD_VALUE);
	return XST_SUCCESS;
}

int XEmacPs_SetupIntrSystem(XScuGic *IntcInstancePtr,
							XScuTimer *TimerInstancePtr,
							XLlFifo *FifoInstancePtr,
							u16 TimerIntrId,
							u16 FifoIntrId)
{
	int Status = XST_SUCCESS;
	XScuGic_Config *GicConfig;

	Xil_ExceptionInit();

	/*
	 * Initialize the interrupt controller driver so that it is ready to
	 * use.
	 */
	GicConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
	if (NULL == GicConfig)
	{
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize(&IntcInstance, GicConfig,
								   GicConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS)
	{
		return XST_FAILURE;
	}
	/*
	 * Connect the interrupt controller interrupt handler to the hardware
	 * interrupt handling logic in the processor.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_IRQ_INT,
								 (Xil_ExceptionHandler)XScuGic_InterruptHandler,
								 IntcInstancePtr);

	/*
	 * Connect the handler for timer interrupt that will be called when the
	 * timer.expires.
	 */
	Status = XScuGic_Connect(IntcInstancePtr, TimerIntrId,
							 (Xil_ExceptionHandler)XEmacPs_TimerInterruptHandler,
							 (void *)&FifoInstance);
	if (Status != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	Status = XScuGic_Connect(IntcInstancePtr, FifoIntrId,
							 (Xil_InterruptHandler)FifoHandler,
							 FifoInstancePtr);
	if (Status != XST_SUCCESS)
	{
		return Status;
	}

	XScuGic_Enable(IntcInstancePtr, TimerIntrId);
	XScuGic_Enable(IntcInstancePtr, FifoIntrId);

	Xil_ExceptionEnableMask(XIL_EXCEPTION_IRQ);

	return XST_SUCCESS;
}

void ReceiveDataValidation(char data)
{


	if (data != CompareByteData)
	{
		xil_printf("[%02X|%02X]", data, CompareByteData);
		CompareByteData = data + 1;
	}
	else
		CompareByteData++;
}


int initialize_dds_tx()
{

	uint32_t read_powerdown=0;
	uint32_t read_dds_pll_lock=0;
	uint32_t read_dac_calib=0;
	uint32_t read_frequency_list_depth=0;
	uint32_t read_dds_readback_register_1=0;
	uint32_t read_dds_readback_register_2=0;
	uint32_t read_dds_readback_register_3=0;

	uint32_t read_hclk_trigger_edge=0;
	uint32_t read_dds_single_frequency_mode=0;
	uint32_t read_tx_program_value=0;
	uint32_t read_rx_program_value=0;
	uint32_t read_dds_hop_rate=0;
	uint32_t read_dds_hop_table_read_addr_reset=0;
	uint32_t read_dds_hop_mode=0;
	//uint32_t *dds_register_map[MAX_DDS_REGISTER_READ_ADDRESS];


	u32 frequency_list_depth=4;
	u32 frequency_table [MAX_FREQUENCY_LIST_DEPTH]= {171798692,257698038,343597384,429496730};

	u8 Status;



	 usleep(500);
	 // DDS Power down API
	 Status= dds_axi_set_power_down(DDS_TX_BASE_ADDR,0,&read_powerdown);
	 usleep(500);
	 usleep(500);
	 Status= dds_axi_read_pll_lock_status(DDS_TX_BASE_ADDR,&read_dds_pll_lock);
		if (Status==0){
			printf("AXI_INFO:DDS PLL Lock Status:");
			printf("%" PRIu32 "\n\r",read_dds_pll_lock);
		}
		else {
			return XST_FAILURE;
		}
     if (read_dds_pll_lock==1){
    	 printf("\n");
    	 printf("\n");
    	 printf("\n");
    	 printf("SYSTEM_INFO:DDS PLL is 'LOCKED'\n");
    	 printf("\n");
    	 printf("\n");
    	 printf("\n");
     }
     else {
    	 printf("SYSTEM_INFO:DDS PLL lock is not detected, terminating DDS initialization\n");
    	 printf("\n");
    	 printf("\n");
    	 printf("\n");
    	 return XST_FAILURE;
     }
     Status= dds_axi_set_power_down(DDS_TX_BASE_ADDR,15,&read_powerdown);
     if (Status==0){
     	printf("AXI_INFO:DDS Power down set:");
     	printf("%" PRIu32 "\n\r",read_powerdown);
     	}
     	else {
     		return XST_FAILURE;
     	}
     sleep(10);
     Status= dds_axi_set_power_down(DDS_TX_BASE_ADDR,0,&read_powerdown);
     if (Status==0){
     	printf("AXI_INFO:DDS Power down status:");
     	printf("%" PRIu32 "\n\r",read_powerdown);
     	}
     	else {
     		return XST_FAILURE;
     	}

     sleep(10);
	 Status= dds_axi_dac_calibration_register(DDS_TX_BASE_ADDR,1,&read_dac_calib);
	 usleep(500);
	 usleep(500);
/*
	 Status= dds_axi_write_custom_register(DDS_TX_BASE_ADDR, 0, 12);
     // Set Hopping frequency list depth i.e number of hop frequencies.
	 Status= dds_axi_frequency_list_depth(DDS_TX_BASE_ADDR,frequency_list_depth,&read_frequency_list_depth);
     usleep(500);
     // Update Hopping Frequency List, writes frequency list to memory. Make sure to update the frequency list till frequency list depth. The maximum frequency list depth available is 256
     Status= dds_axi_update_hopping_frequency_list(DDS_TX_BASE_ADDR,frequency_table,frequency_list_depth);
     usleep(500);
     int* read_frequency_table= dds_axi_read_frequency_table_memory(DDS_TX_BASE_ADDR,frequency_list_depth);
     for ( int i = 0; i < frequency_list_depth; i++ ) {
           printf( "read_frequency_table : %d %d\n", i, read_frequency_table[i]);
        }
     sleep(10);
     // AXI4-Lite control driver to read back the AD9914 DDS registers values. After FPGA boots up it initially programs AD9914 over SPI and then readback all register which stores in BRAM.
     // This control driver read back the BRAM contents which is equivalent copy of DDS registers
     int* dds_register_map= dds_axi_read_register_memory(DDS_TX_BASE_ADDR);
          for ( int i = 0; i < MAX_DDS_REGISTER_READ_ADDRESS; i++ ) {
                printf( "read_dds_register_map : Address 0x%02X: 0x%08X\n", i, dds_register_map[i]);
             }

     sleep(10);
     // General purpose readback register-1
	 Status= dds_axi_read_readback_register_1(DDS_TX_BASE_ADDR, &read_dds_readback_register_1);
     usleep(500);
     // General purpose readback register-2
     Status= dds_axi_read_readback_register_2(DDS_TX_BASE_ADDR, &read_dds_readback_register_2);
     usleep(500);
     // General purpose readback register-3
     Status= dds_axi_read_readback_register_3(DDS_TX_BASE_ADDR, &read_dds_readback_register_3);
     usleep(500);
     // Set Clock edge on which hopping frequency to be updated. HOP_CLOCK_EDGE_VALUE =>1 means Rising Edge of Hop Clock and HOP_CLOCK_EDGE_VALUE =>0 means Falling Edge of Hop Clock
     Status= dds_axi_set_hop_clock_trigger_edge(DDS_TX_BASE_ADDR,0,&read_hclk_trigger_edge);
     usleep(500);
     // Make sure the single frequency mode value is set to 0 for Frequency Hopping Mode
     Status= dds_axi_set_single_frequency_mode(DDS_TX_BASE_ADDR,0,&read_dds_single_frequency_mode);
     usleep(500);
     // Program any default value to Transmit DDS frequency value. This value to be used in Single Frequency Mode only otherwise ignored.
     Status= dds_axi_set_tx_program_value(DDS_TX_BASE_ADDR,64,&read_tx_program_value);
     usleep(500);
     // Program any default value to Receive DDS frequency value. This value to be used in Single Frequency Mode only otherwise ignored.
     Status= dds_axi_set_rx_program_value(DDS_TX_BASE_ADDR,64,&read_rx_program_value);
     usleep(500);
     // Set DDS Hopping Rate. The maximum Hopping Rate supported is 1000 Hops Per Seconds. HOP_RATE can be computed as 1000/number of hops per second.
     // Lets take we want to update every 100 ms i.e. 10 frequency updates per second. The HOP_RATE can be computed as 1000/10 = 100
     Status= dds_axi_set_hopping_rate(DDS_TX_BASE_ADDR, 100, &read_dds_hop_rate);
     usleep(500);            //Short Delay
     // Set START_HOP to 1 to start hopping at the set HOP_RATE
     Status= dds_axi_set_start_stop_hop(DDS_TX_BASE_ADDR, 1, &read_dds_hop_mode);
     sleep(50);         // Long Delay
     // Set START_HOP to 0 to stop hopping.
     Status= dds_axi_set_start_stop_hop(DDS_TX_BASE_ADDR, 0, &read_dds_hop_mode);
     sleep(50);         // Long Delay
     // Lets take we want to update every 500 ms i.e. 2 frequency updates per second. The HOP_RATE can be computed as 1000/2 = 500
     Status= dds_axi_set_hopping_rate(DDS_TX_BASE_ADDR, 500, &read_dds_hop_rate);
     usleep(500);      // Short Delay
     // Set START_HOP to 1 to start hopping at the set HOP_RATE
     Status= dds_axi_set_start_stop_hop(DDS_TX_BASE_ADDR, 1, &read_dds_hop_mode);
     sleep(50);        // Long Delay
     // Set START_HOP to 0 to stop hopping.
     Status= dds_axi_set_start_stop_hop(DDS_TX_BASE_ADDR, 0, &read_dds_hop_mode);
     usleep(500);      // Short Delay
     // Driver to reset hopping frequency memory read address to 0.
     Status= dds_axi_hop_table_address_reset(DDS_TX_BASE_ADDR, 1, &read_dds_hop_table_read_addr_reset);
     usleep(500);      // Short Delay
     // Release hopping memory read address reset.
     Status= dds_axi_hop_table_address_reset(DDS_TX_BASE_ADDR, 0, &read_dds_hop_table_read_addr_reset);
     sleep(50);        // Long Delay
     // Lets take we want to update every 1000 ms i.e. 1 frequency updates per second. The HOP_RATE can be computed as 1000/1 = 1000
     Status= dds_axi_set_hopping_rate(DDS_TX_BASE_ADDR, 1000, &read_dds_hop_rate);
     usleep(500);      // Short Delay
     // Set START_HOP to 1 to start hopping at the set HOP_RATE
     Status= dds_axi_set_start_stop_hop(DDS_TX_BASE_ADDR, 1, &read_dds_hop_mode);
     sleep(50);        // Long Delay
     // Set START_HOP to 0 to stop hopping.
     Status= dds_axi_set_start_stop_hop(DDS_TX_BASE_ADDR, 0, &read_dds_hop_mode);
     sleep(50);        // Long Delay
*/
     // Individual DDS frequency value programming using program registers for single frequency mode
     Status= dds_axi_set_single_frequency_mode(DDS_TX_BASE_ADDR, 1, &read_dds_single_frequency_mode);
     // Program Value 1 (Some random Value)
    Status= dds_axi_set_tx_program_value(DDS_TX_BASE_ADDR, 343597384, &read_tx_program_value);
//    sleep(50);          // Long Delay

    // Program Value 2 (Some random Value)
//    Status= dds_axi_set_tx_program_value(DDS_TX_BASE_ADDR, 429496730, &read_tx_program_value);
//    sleep(50);         // Long Delay

    Status= dds_axi_set_power_down(DDS_TX_BASE_ADDR,16,&read_powerdown);

    // Individual DDS frequency value programming using program registers for single frequency mode
//     Status= dds_axi_set_single_frequency_mode(DDS_TX_BASE_ADDR, 0, &read_dds_single_frequency_mode);
return XST_SUCCESS;
}


int initialize_pl_modem()
{

	uint32_t transmit_message_type=0;
	uint32_t transmit_continuous=0;
	uint32_t transmit_symbol_rate=0;
	uint32_t receive_symbol_rate=0;
	uint32_t transmit_modulation_select=0;
	uint32_t receive_modulation_select=0;
	//uint32_t dds_freq_list_depth_read=0;

	uint32_t idle_mode=0;
	uint32_t read_version_reg=0;
	uint32_t read_changelist=0;
	u8 Status;
	u8 num_control_bytes=5; //0-5
	u32 transmit_control_bytes[num_control_bytes+1];
	u8 version_major=2;
	u8 version_minor=2;
	const u8 modem_version = (version_major<<4) + (version_minor<<0);
	u32 CHANGELIST=191790038;
	//u8  MODEM_CONFIG_NUM=0; // Supported from 0 to 3
	//u32 DDS_FREQ_LIST_DEPTH= 128;


	gpio_control_system_reset(&Gpio);

#ifdef MODEM_CONFIG
	gpio_control_modem_configuration(&Gpio, MODEM_CONFIG_NUM);
#endif
#ifdef MASTER
	gpio_control_master_slave(&Gpio);
#endif


	Status = transmit_axi_version_register(TRANSMIT_CTRL_PHY_BASE_ADDR,modem_version,&read_version_reg);
		if (Status==0){
			printf("AXI_INFO:Version Register passed:FHRRv");
			printf("%" PRIu32".""%"PRIu32 "\n\r",(read_version_reg>>4),(read_version_reg&0xF));
			}

	Status = transmit_axi_changelist(TRANSMIT_CTRL_PHY_BASE_ADDR,CHANGELIST,&read_changelist);
		if (Status==0){
	     	printf("AXI_INFO:ChangeList Value:");
			printf("%" PRIu32 "\n\r",read_changelist);
			}

	//Initialize Control Bytes for Transmit Register
	 for (int i=0; i<=num_control_bytes;i++){
		 transmit_control_bytes[i] = (i*2);
	 }

	/* PL-Modem AXI-Read/Write Example Code of using APIs*/
    //Write All 6-Bytes of Control Data
	Status= transmit_control_byte_N(TRANSMIT_CTRL_PHY_BASE_ADDR,transmit_control_bytes,num_control_bytes);
		if (Status==0){
			printf("AXI_INFO:IDLE Mode value:");
			printf("%" PRIu32 "\n\r",idle_mode);
		}
    // Write IDLE-Mode Enable to Transmit Path
	Status= transmit_axi_idle_mode_enable(TRANSMIT_CTRL_PHY_BASE_ADDR,TRANSMIT_IDLE_MODE_EN,&idle_mode);
	if (Status==0){
		printf("AXI_INFO:IDLE Mode value:");
		printf("%" PRIu32 "\n\r",idle_mode);
	}
    // Write Tx Message Type for PS data
	Status= transmit_axi_message_type(TRANSMIT_CTRL_PHY_BASE_ADDR,TRANMSIT_MESSAGE_MODE,&transmit_message_type);
	if (Status==0){
		printf("AXI_INFO:Transmit Message Type value:");
		printf("%" PRIu32 "\n\r",transmit_message_type);
	}
    // Write Transmit Mode for Continuous transmission i.e. for transmitter enable
	Status= transmit_axi_transmit_continuous(TRANSMIT_CTRL_PHY_BASE_ADDR,TRANSMIT_CONTINUOUS_MODE,&transmit_continuous);
	if (Status==0){
		printf("AXI_INFO:Transmit Continuous value:");
		printf("%" PRIu32 "\n\r",transmit_continuous);
	}


    // Read Mode for Transmit Symbol Rate i.e. for symbol rate value
	Status= transmit_axi_symbol_rate(TRANSMIT_CTRL_PHY_BASE_ADDR,-1,&transmit_symbol_rate);
	if (Status==0){
		printf("AXI_INFO:Transmit symbol rate value:");
		printf("%" PRIu32 "\n\r",transmit_symbol_rate);
	}
	Status= receive_axi_symbol_rate(RECEIVE_CTRL_PHY_BASE_ADDR,-1,&receive_symbol_rate);
		if (Status==0){
			printf("AXI_INFO:Receive symbol rate value:");
			printf("%" PRIu32 "\n\r",receive_symbol_rate);
		}
	Status= transmit_axi_modulation_select(TRANSMIT_CTRL_PHY_BASE_ADDR,-1,&transmit_modulation_select);
		if (Status==0){
			printf("AXI_INFO:Transmit Modulation Select value:");
			printf("%" PRIu32 "\n\r",transmit_modulation_select);
		}
	Status= receive_axi_modulation_select(RECEIVE_CTRL_PHY_BASE_ADDR,-1,&receive_modulation_select);
		if (Status==0){
			printf("AXI_INFO:Receive Modulation Select value:");
			printf("%" PRIu32 "\n\r",receive_modulation_select);
		}



	return XST_SUCCESS;
}

void XEmacPs_RunIEEE1588Protocol()
{
	u32 ReceiveLength, i = 0, j = 0;
	u32 TxWord[1500];
	u32 RxWord[256];
	u8 Status;
	u16 var_packet_length = 0;
	u32 rx_test_word;
	uint32_t read_dds_hop_rate=0;
	uint32_t read_dds_hop_table_read_addr_reset=0;
    uint32_t read_dds_hop_mode=0;
    uint32_t test_control_array[6] = {0};
    uint8_t test_control = 0;
	sleep(1);
//	xil_printf( "Initializing AD9914\n" );
//	ad9914_init();
	for (i = 1; i > 0; i--)
	{
		sleep(1);
		xil_printf("SYSTEM_INFO:Starting Application in %d Sec\r\n", i);
	}
	xil_printf("--------------------------------------------------\r\n", i);
	initialize_pl_modem();
	xil_printf( "Initializing AD9914\n" );
	for (i = 5; i > 0; i--)
	{
		sleep(1);
		xil_printf("SYSTEM_INFO:Initialize DDS-Tx in %d Sec\r\n", i);
	}
	initialize_dds_tx();
	XScuTimer_Start(&TimerInstance);
	CompareByteData = 0;
	PrevChunkNum = 0;
//	Status= dds_axi_hop_table_address_reset(DDS_TX_BASE_ADDR, 1, &read_dds_hop_table_read_addr_reset);
//	usleep(500);      // Short Delay
//	// Release hopping memory read address reset.
//	Status= dds_axi_hop_table_address_reset(DDS_TX_BASE_ADDR, 0, &read_dds_hop_table_read_addr_reset);
//	//sleep(50);        // Long Delay
//	// Lets take we want to update every 1000 ms i.e. 1 frequency updates per second. The HOP_RATE can be computed as 1000/1 = 1000
//	Status= dds_axi_set_hopping_rate(DDS_TX_BASE_ADDR, 1, &read_dds_hop_rate);
//	usleep(500);      // Short Delay
//	// Set START_HOP to 1 to start hopping at the set HOP_RATE
//	Status= dds_axi_set_start_stop_hop(DDS_TX_BASE_ADDR, 1, &read_dds_hop_mode);
	//sleep(50);        // Long Delay
	//// Set START_HOP to 0 to stop hopping.
	//Status= dds_axi_set_start_stop_hop(DDS_TX_BASE_ADDR, 0, &read_dds_hop_mode);
while (1)

	{
#ifdef TRANSMITTER
		if (Tick)
		{
			//Initialize Control Bytes for Transmit Register
			for (int i = 0; i < 6; i++) {
				test_control_array[i] = test_control;
			}
			test_control++;
			/* PL-Modem AXI-Read/Write Example Code of using APIs*/
			//Write All 6-Bytes of Control Data
			write_control_bytes(test_control_array, 6);

		  if(PacketCounter<= (NUM_PACKETS-1)){
			for (j = 0; j < TRANSFER_PACKETS; j++)
			{
				var_packet_length = PACKET_LENGTH;
				for (i = 0; i < (var_packet_length / 4); i++)
				{
					TxWord[i] = (4*i) | ((4*i+1) << 8) | ((4*i+2) << 16) | ((4*i+3) << 24);

				}
				Status = TxSend(&FifoInstance, TxWord, var_packet_length / 4);
				usleep(500);
				if (Status == XST_FAILURE)
				{
					xil_printf("S%d ", j);
					break;
				}
				TransferWord++;
				TransmittedCntTotal++;
			}
			Tick = 0;
			PacketCounter++;

		  }
		}
#endif
#ifdef RECEIVER
		Occu = XLlFifo_iRxOccupancy(&FifoInstance);
		if (Occu)
		{
			ReceiveLength = (XLlFifo_iRxGetLen(&FifoInstance));
			if (ReceiveLength)
			{
				ReceivedCntTotal++;
				for (i = 0; i < ReceiveLength / 4; i++) {
					RxWord[i] = XLlFifo_RxGetWord(&FifoInstance);
				}
				for (i = 0; i < ReceiveLength / 4; i++) {
					rx_test_word = (4*i) | ((4*i+1) << 8) | ((4*i+2) << 16) | ((4*i+3) << 24);
					if(RxWord[i] != rx_test_word) {
						ErrCnt++;
					}
				}
			}
		}
#endif
	}
}
/*****************************************************************************/
/**
Timer Interrupt Handler
******************************************************************************/
void XEmacPs_TimerInterruptHandler()
{

	if ((TickCount % ((0x1 << (TIMER_DIVIDER - 1)))) == 0)
	{
		ReceivedCnt = 0;
		FailedPacket = 0;
		xil_printf("\r\nSTATS:Inst#%04d Tx:%04d Rx:%04d Err:%04d",instanceCnt, TransmittedCntTotal,ReceivedCntTotal,ErrCnt);
		TransmittedCntTotal=0;
		ReceivedCntTotal = 0;
		ReceiveDataLength = 0;
		TickCount = 0;
		//ErrCnt = 0; //This will restore previous error count. Do not flush
	}
	TickCount++;
	instanceCnt++;
	Tick = 1;
	Print = 1;


}

void write_control_bytes(uint32_t *value, uint8_t number_of_bytes) {
	u8 i=0;
	for (i=0; i<number_of_bytes; i++)
	{
		Xil_Out32(TRANSMIT_CTRL_PHY_BASE_ADDR + TRANSMIT_CONTROL_BYTE_0_OFFSET+(i<<2), value[i]);
	}
}
