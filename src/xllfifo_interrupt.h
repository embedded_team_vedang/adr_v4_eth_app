#include "xscugic.h"
#include "xllfifo.h"

int SetupFIFO(XLlFifo *InstancePtr, u16 DeviceId);
//int XLlFifoInterruptExample(XLlFifo *InstancePtr, u16 DeviceId);
int TxSend(XLlFifo *InstancePtr, u32 *SourceAddr, u16 DataLen);
int TxSendByte(XLlFifo *InstancePtr, u8 *SourceAddr, u16 DataLen);
void FifoHandler(XLlFifo *Fifo);
void FifoRecvHandler(XLlFifo *Fifo);
void FifoSendHandler(XLlFifo *Fifo);
void FifoErrorHandler(XLlFifo *InstancePtr, u32 Pending);
int SetupInterruptSystem(XScuGic *IntcInstancePtr, XLlFifo *InstancePtr,
				u16 FifoIntrId);
void DisableIntrSystem(XScuGic *IntcInstancePtr, u16 FifoIntrId);
int PushPacketInRAM(XLlFifo *InstancePtr, u8  *data, u16 DataLen, u8 PacketNum);
int PopPacketFromRAM(u32 *ReceivedData);
