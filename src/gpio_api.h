/**
 * @file     GPIO_Control.h
 * @brief    GPIO Control for different PL configurations.
 * @date     June 16, 2019
 */

//#ifndef GPIO_CTRL_H
//#define GPIO_CTRL_H

#include "xparameters.h"
#include "xil_types.h"
#include "stdio.h"
#include "xgpio.h"


#define GPIO_TRI_IO_0_CHANNEL 1
#define GPIO_0_EXAMPLE_DEVICE_ID  XPAR_GPIO_0_DEVICE_ID

int gpio_control_system_reset(XGpio *InstancePtr);
int gpio_control_register_init(XGpio *InstancePtr);
int gpio_control_master_slave(XGpio *InstancePtr);
int gpio_control_modem_configuration(XGpio *InstancePtr, u16 configuration_id);
