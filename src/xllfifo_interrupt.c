//
//#include "xstreamer.h"
//#include "xil_cache.h"
#include "xllfifo.h"
#include "xstatus.h"
#include "xscugic.h"
#include "common_header.h"

#define FIFO_DEV_ID XPAR_AXI_FIFO_0_DEVICE_ID
#define INTC_DEVICE_ID XPAR_SCUGIC_SINGLE_DEVICE_ID
#define FIFO_INTR_ID XPAR_FABRIC_LLFIFO_0_VEC_ID

#define GET_CHUNK_NO(x) (u8)((x & 0xF0000000) >> 28)
#define GET_TOTALCHUNK_NO(x) (u8)((x & 0x0F000000) >> 24)
#define GET_PACKET_NO(x) (u8)((x & 0x00FF0000) >> 16)
#define GET_PACKET_DATA_LENGTH(x) (u16)(x & 0x0000FFFF)

unsigned int send_busy_flag;

u16 DropPacketCount = 0;
u16 ReceivedPacket = 0;
u8 ReceivedFrame;

u8 RawPacketBuffer[MAX_ETHERNET_BYTES];

int SetupFIFO(XLlFifo *InstancePtr, u16 DeviceId)
{
	XLlFifo_Config *Config;
	int Status;
	Status = XST_SUCCESS;

	/* Initialize the Device Configuration Interface driver */
	Config = XLlFfio_LookupConfig(DeviceId);
	if (!Config)
	{
		xil_printf("No config found for %d\r\n", DeviceId);
		return XST_FAILURE;
	}

	/*
	 * This is where the virtual address would be used, this example
	 * uses physical address.
	 */
	Status = XLlFifo_CfgInitialize(InstancePtr, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS)
	{
		xil_printf("Initialization failed\n\r");
		return Status;
	}

	/* Check for the Reset value */
	Status = XLlFifo_Status(InstancePtr);
	XLlFifo_IntClear(InstancePtr, 0xffffffff);
	Status = XLlFifo_Status(InstancePtr);
	if (Status != 0x0)
	{
		xil_printf("\n ERROR : Reset value of ISR0 : 0x%x\t"
				   "Expected : 0x0\n\r",
				   XLlFifo_Status(InstancePtr));
		return XST_FAILURE;
	}
	return Status;
}

///******************************************************************************/
void FifoRecvHandler(XLlFifo *InstancePtr)
{
	ReceivedPacket++;
	ReceivedFrame = 1;
}

///*****************************************************************************/
void FifoSendHandler(XLlFifo *InstancePtr)
{
	XLlFifo_IntClear(InstancePtr, XLLF_INT_TC_MASK);
	send_busy_flag = 0;
}

///*****************************************************************************/
void FifoErrorHandler(XLlFifo *InstancePtr, u32 Pending)
{
	if (Pending & XLLF_INT_RPURE_MASK)
	{
		XLlFifo_RxReset(InstancePtr);
	}
	else if (Pending & XLLF_INT_RPORE_MASK)
	{
		XLlFifo_RxReset(InstancePtr);
	}
	else if (Pending & XLLF_INT_RPUE_MASK)
	{
		XLlFifo_RxReset(InstancePtr);
	}
	else if (Pending & XLLF_INT_TPOE_MASK)
	{
		XLlFifo_TxReset(InstancePtr);
	}
	else if (Pending & XLLF_INT_TSE_MASK)
	{
	}
}

///*******************************************************************************/
void FifoHandler(XLlFifo *InstancePtr)
{
	u32 Pending;

	Pending = XLlFifo_IntPending(InstancePtr);
	while (Pending)
	{
		if (Pending & XLLF_INT_RC_MASK)
		{
			FifoRecvHandler(InstancePtr);
			XLlFifo_IntClear(InstancePtr, XLLF_INT_RC_MASK);
		}
		else if (Pending & XLLF_INT_TC_MASK)
		{
			FifoSendHandler(InstancePtr);
		}
		else if (Pending & XLLF_INT_ERROR_MASK)
		{
			FifoErrorHandler(InstancePtr, Pending);
			XLlFifo_IntClear(InstancePtr, XLLF_INT_ERROR_MASK);
		}
		else
		{
			XLlFifo_IntClear(InstancePtr, Pending);
		}
		Pending = XLlFifo_IntPending(InstancePtr);
	}
}

///*****************************************************************************/
int SetupInterruptSystem(XScuGic *IntcInstancePtr,
						 XLlFifo *InstancePtr,
						 u16 FifoIntrId)
{

	int Status;

	XScuGic_Config *IntcConfig;

	/*
	 * Initialize the interrupt controller driver so that it is ready to
	 * use.
	 */
	IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
	if (NULL == IntcConfig)
	{
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig,
								   IntcConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	XScuGic_SetPriorityTriggerType(IntcInstancePtr, FifoIntrId, 0xA0, 0x3);

	/*
	 * Connect the device driver handler that will be called when an
	 * interrupt for the device occurs, the handler defined above performs
	 * the specific interrupt processing for the device.
	 */
	Status = XScuGic_Connect(IntcInstancePtr, FifoIntrId,
							 (Xil_InterruptHandler)FifoHandler,
							 InstancePtr);
	if (Status != XST_SUCCESS)
	{
		return Status;
	}

	XScuGic_Enable(IntcInstancePtr, FifoIntrId);

	/*
	 * Initialize the exception table.
	 */
	Xil_ExceptionInit();

	/*
	 * Register the interrupt controller handler with the exception table.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
								 (Xil_ExceptionHandler)XScuGic_InterruptHandler,
								 (void *)IntcInstancePtr);
	;

	/*
	 * Enable exceptions.
	 */
	Xil_ExceptionEnable();

	return XST_SUCCESS;
}
/****************Add Escape Frames **************************/
unsigned int EscapeFrame(u8 *INbuff, u8 *OUTbuff, u16 Buff_Length)
{
	unsigned int i, OutBuffLen;
	OutBuffLen = 0x00;
	for (i = 0; i < Buff_Length; i++)
	{
		if (INbuff[i] == START_FRAME)
		{
			OutBuffLen++;
			*OUTbuff++ = INbuff[i];
			OutBuffLen++;
			*OUTbuff++ = INbuff[i];
		}
		else
		{
			*OUTbuff++ = INbuff[i];
			OutBuffLen++;
		}
	}
	return OutBuffLen;
}

/***************************************************************************/
int TxSend(XLlFifo *InstancePtr, u32 *SourceAddr, u16 DataLen)
{
	int j;
	send_busy_flag = 1;
	if (XLlFifo_iTxVacancy(InstancePtr) < ((DataLen + 1) * WORD_SIZE))
	{
		xil_printf("\r\n--Skip--\r\n");
		return XST_FAILURE;
	}
	else
	{
		for (j = 0; j < DataLen; j++)
		{
			XLlFifo_TxPutWord(InstancePtr, SourceAddr[j]);
		}
	}

	XLlFifo_iTxSetLen(InstancePtr, ((DataLen)*WORD_SIZE));

	return XST_SUCCESS;
}

/***************************************************************************/
int TxSendByte(XLlFifo *InstancePtr, u8 *PacketSourceAddr, u16 DataLen)
{
	int j;
	unsigned int packets;
	u32 word;
	u8 SourceAddr[MAX_ETHERNET_BYTES];

	DataLen = EscapeFrame(PacketSourceAddr, SourceAddr, DataLen);
	//	Clear Next 4 Bytes to avoid garbage value in packet
	SourceAddr[DataLen] = 0x00;
	SourceAddr[DataLen + 1] = 0x00;
	SourceAddr[DataLen + 2] = 0x00;
	SourceAddr[DataLen + 3] = 0x00;

	xil_printf("%d", (DataLen & 0x3));
	packets = (DataLen & 0x3) ? (DataLen >> 2) + 1 : DataLen >> 2;
	xil_printf("Transmitting Data ... %3d %3d\r\n", DataLen, packets);
	SourceAddr[(packets * 4) - 1] = 0x02;
	for (j = 0; j < packets * 4; j = j + 4)
	{
		if (XLlFifo_iTxVacancy(InstancePtr))
		{
			word = (SourceAddr[j] << 24) + (SourceAddr[j + 1] << 16) + (SourceAddr[j + 2] << 8) + (SourceAddr[j + 3]);
			xil_printf("word %8X\r\n", word);
			XLlFifo_TxPutWord(InstancePtr, word);
		}
	}

	/* Start Transmission by writing transmission length into the TLR */
	XLlFifo_iTxSetLen(InstancePtr, (packets * 4));
	xil_printf("XLlFifo_iTxVacancy = %d \r\n", XLlFifo_iTxVacancy(InstancePtr));

	/* Transmission Complete */
	return XST_SUCCESS;
}

void TxSendPrint(XLlFifo *InstancePtr, u32 *SourceAddr, u16 DataLen)
{
	u16 i;
	for (i = 0; i < DataLen; i++)
	{
		if ((i % 16) == 0)
			xil_printf("\r\n ...%04d  ", i);
		xil_printf("  %08X  ", SourceAddr[i]);
	}
	xil_printf("\r\n");
}

//void PushPacketInRAM(ring_t *RingBuffer, const char *data, int Size, unsigned int PacketNum)
int PushPacketInRAM(XLlFifo *InstancePtr, u8 *PacketData, u16 DataLen, u8 PacketNum)
{
	unsigned int i, ptr, chunk, TotalChunks, Status;
	long unsigned int buffdata[PACKET_LENGTH / 4];
	u8 data[MAX_ETHERNET_BYTES];

	DataLen = EscapeFrame(PacketData, data, DataLen);
	//	Clear Next 4 Bytes to avoid garbage value in packet
	data[DataLen] = 0x00;
	data[DataLen + 1] = 0x00;
	data[DataLen + 2] = 0x00;
	data[DataLen + 3] = 0x00;

	TotalChunks = ((DataLen - 1) / (PACKET_LENGTH - OVERHEAD_BYTES)) + 1;
#ifdef PRINT_TRANSMIT_CHUNK_LABEL
	xil_printf("RX P %03d DL %04d C %d\r\n", PacketNum, DataLen, TotalChunks);
#endif
	ptr = 0;
	for (chunk = 0; chunk < TotalChunks; chunk++)
	{
		for (i = 0; i < PACKET_LENGTH / 4; i++)
			buffdata[i] = 0x00000000;
		// Push 256 Byte in Ring Buffer == 64 unsigned long
		for (i = 0; i < PACKET_LENGTH / 4; i++)
		{
			if ((chunk == 0) && (i == 0)) // If First Chuck First Long
				buffdata[i] = 0x00000000 | ((((0x0F & (chunk + 1)) << 4) | (0x0F & TotalChunks)) << 24) | (PacketNum << 16) | (DataLen << 0);
			else if (ptr > DataLen)
				buffdata[i] = 0x00000000;
			else if (i == 0)
			{
				buffdata[i] = 0x00000000 | ((((0x0F & (chunk + 1)) << 4) | (0x0F & TotalChunks)) << 24) | (PacketNum << 16) | (data[ptr] << 8) | (data[ptr + 1] << 0);
				ptr = ptr + 2;
			}
			else
			{
				buffdata[i] = 0x00000000 | (data[ptr] << 24) | (data[ptr + 1] << 16) | (data[ptr + 2] << 8) | (data[ptr + 3] << 0);
				ptr = ptr + 4;
			}
		}

#ifdef PRINT_TRANSMISSION_PACKET_LABEL
		xil_printf("**** Submitting Chunk %03d/%03d of %03d ", chunk, TotalChunks, PacketNum);
		if (GET_CHUNK_NO(buffdata[0]) == 1)
			xil_printf("Length %d->%d ***\r\n", DataLen, GET_PACKET_DATA_LENGTH(buffdata[0]));
		else
			xil_printf("*** \r\n");
#endif
#ifdef PRINT_TRANSMISSION_PACKET_DATA
		TxSendPrint(InstancePtr, buffdata, (u16)PACKET_LENGTH / 4);
#endif
		Status = TxSend(InstancePtr, buffdata, (u16)PACKET_LENGTH / 4);
		if (Status != XST_SUCCESS)
		{
			return XST_FAILURE;
		}
	}
	return XST_SUCCESS;
}

/******************************************************************************/
void DisableIntrSystem(XScuGic *IntcInstancePtr, u16 FifoIntrId)
{
	XScuGic_Disconnect(IntcInstancePtr, FifoIntrId);
}
