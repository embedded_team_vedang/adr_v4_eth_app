/**
 * @file     axi.h
 * @brief    Contains addresses of Registers which are interfaced using AXI4-Lite interface with PL Modem's Physical Layer.
 * @date     June 16, 2019
 */

#ifndef AXI_H
#define AXI_H

#include "xparameters.h"
#include "xil_types.h"
#include "stdio.h"

#define PHY_BASE_ADDR XPAR_ZYNQ_PROC_DESIGN_DESIGN_1_I_S_AXI_PS_PHY_BASEADDR

#define TRANSMIT_CTRL_PHY_BASE_ADDR				PHY_BASE_ADDR + (0x0001<<12)
#define RECEIVE_CTRL_PHY_BASE_ADDR				PHY_BASE_ADDR

#define TRANSMIT_SYMBOL_RATE_OFFSET				(0x0001 << 2)
#define TRANSMIT_MODULATION_SELECT_OFFSET		(0x0002 << 2)
#define TRANSMIT_DELAY_HCLK_RESET_OFFSET		(0x0003 << 2)
#define TRANSMIT_START_PULSE_WIDTH_OFFSET		(0x0004 << 2)
#define TRANSMIT_START_DELAY_OFFSET				(0x0005 << 2)
#define TRANSMIT_MESSAGE_TYPE_OFFSET			(0x0006 << 2)
#define TRANSMIT_REQUEST_OFFSET					(0x0007 << 2)
#define TRANSMIT_CONTINUOUS_OFFSET				(0x0008 << 2)
#define TRANSMIT_FRAME_COUNTER_RESET_OFFSET		(0x0009 << 2)
#define TRANSMIT_PACKET_TYPE_OFFSET				(0x000A << 2)
#define TRANSMIT_SET_NUM_PACKETS_OFFSET			(0x000B << 2)

#define TRANSMIT_FRAME_COUNT_OFFSET				(0x000D << 2)
#define TRANSMIT_SCRAMBLER_ENABLE_OFFSET    	(0x000E << 2)
#define TRANSMIT_FEC_SELECT_OFFSET				(0x000F << 2)
#define TRANSMIT_CONTROL_BYTE_0_OFFSET   		(0x0010 << 2)
#define TRANSMIT_CONTROL_BYTE_1_OFFSET   		(0x0011 << 2)
#define TRANSMIT_CONTROL_BYTE_2_OFFSET   		(0x0012 << 2)
#define TRANSMIT_CONTROL_BYTE_3_OFFSET   		(0x0013 << 2)
#define TRANSMIT_CONTROL_BYTE_4_OFFSET   		(0x0014 << 2)
#define TRANSMIT_CONTROL_BYTE_5_OFFSET   		(0x0015 << 2)
#define TRANSMIT_DUMMY_DATA_OFFSET   			(0x0016 << 2)
#define TRANSMIT_IDLE_MODE_ENABLE_OFFSET   		(0x0017 << 2)
#define TRANSMIT_VERSION_REGISTER_OFFSET   		(0x0018 << 2)
#define TRANSMIT_CHANGELIST_OFFSET   		    (0x0019 << 2)

#define RECEIVE_SYMBOL_RATE_OFFSET				(0x0001 << 2)
#define RECEIVE_MODULATION_SELECT_OFFSET		(0x0002 << 2)

#define RECEIVE_SCRAMBLER_ENABLE_OFFSET		    (0x000E << 2)
#define RECEIVE_FEC_SELECT_OFFSET				(0x000F << 2)
#define RECEIVE_CONTROL_BYTE_0_OFFSET   		(0x0010 << 2)
#define RECEIVE_CONTROL_BYTE_1_OFFSET   		(0x0011 << 2)
#define RECEIVE_CONTROL_BYTE_2_OFFSET   		(0x0012 << 2)
#define RECEIVE_CONTROL_BYTE_3_OFFSET   		(0x0013 << 2)
#define RECEIVE_CONTROL_BYTE_4_OFFSET   		(0x0014 << 2)
#define RECEIVE_CONTROL_BYTE_5_OFFSET   		(0x0015 << 2)
#define RECEIVE_DUMMY_DATA_OFFSET   			(0x0016 << 2)
#define RECEIVE_IDLE_MODE_ENABLE_OFFSET   		(0x0017 << 2)
#define RECEIVE_VERSION_REGISTER_OFFSET   		(0x0018 << 2)
#define RECEIVE_CHANGELIST_OFFSET   		    (0x0019 << 2)



// DDS AXI-Lite Registers

#define DDS_TX_BASE_ADDR XPAR_ZYNQ_PROC_DESIGN_DESIGN_1_I_S_AXI_DDS_TX_BASEADDR


#define DDS_PLL_LOCK_OFFSET						(0x0001 << 2)
#define DDS_DAC_CALIBRATION_ENABLE_OFFSET		(0x0002 << 2)
#define DDS_FREQUENCY_LIST_DEPTH_OFFSET			(0x0003 << 2)
#define DDS_HCLK_TRIGGER_EDGE_OFFSET			(0x0004 << 2)
#define DDS_POWER_DOWN_OFFSET					(0x0005 << 2)
#define DDS_HOP_START_STOP_OFFSET				(0x0006 << 2)
#define DDS_SINGLE_FREQ_MODE_OFFSET				(0x0007 << 2)
#define DDS_TRANSMIT_FREQUENCY_OFFSET			(0x0008 << 2)
#define DDS_RECEIVE_FREQUENCY_OFFSET			(0x0009 << 2)
#define DDS_HOP_TABLE_READ_ADDRESS_RESET_OFFSET (0x000A << 2)
#define DDS_CUSTOM_ADDRESS_OFFSET				(0x000B << 2)
#define DDS_CUSTOM_DATA_OFFSET			    	(0x000C << 2)
#define DDS_CUSTOM_WRITE_ENABLE_OFFSET			(0x000D << 2)
#define DDS_HOP_RATE_OFFSET						(0x000E << 2)
#define DDS_READBACK_REGISTER_1_OFFSET			(0x000F << 2)
#define DDS_READBACK_REGISTER_2_OFFSET			(0x0010 << 2)
#define DDS_READBACK_REGISTER_3_OFFSET			(0x0011 << 2)

#define DDS_FREQUENCY_TABLE_START_ADDRESS       (0x0400 << 2)
#define DDS_FREQUENCY_TABLE_END_ADDRESS         (0x04FF << 2)

#define DDS_AMPLITUDE_TABLE_START_ADDRESS       (0x0800 << 2)
#define DDS_AMPLITUDE_TABLE_END_ADDRESS         (0x08FF << 2)

#define DDS_AD9914_REGISTER_START_ADDRESS       (0x0C00 << 2)
#define DDS_AD9914_REGISTER_END_ADDRESS         (0x0CFF << 2)
#endif
// All Transmit Side Functions for Register Read/Write; See Function declaration
int transmit_axi_symbol_rate(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_modulation_select(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_delay_hop_clock_reset(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_start_pulse_width(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_start_delay(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_message_type(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_transmit_request(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_transmit_continuous(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_reset_frame_counter(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_packet_type(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_set_num_packets(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_frame_counter(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_scrambler_enable(u32 baseaddr,int value,u32 *read_value);
int transmit_fec_select(u32 baseaddr,int value,u32 *read_value);
int transmit_control_byte_0(u32 baseaddr,int value,u32 *read_value);
int transmit_control_byte_1(u32 baseaddr,int value,u32 *read_value);
int transmit_control_byte_2(u32 baseaddr,int value,u32 *read_value);
int transmit_control_byte_3(u32 baseaddr,int value,u32 *read_value);
int transmit_control_byte_4(u32 baseaddr,int value,u32 *read_value);
int transmit_control_byte_5(u32 baseaddr,int value,u32 *read_value);
int transmit_control_byte_N(u32 baseaddr,u32 value[],u8 num_control_bytes);
int transmit_dummy_data_U32(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_idle_mode_enable(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_version_register(u32 baseaddr,int value,u32 *read_value);
int transmit_axi_changelist(u32 baseaddr,int value,u32 *read_value);


// All Receive Side Functions for Register Read/Write; See Function declaration
int receive_axi_symbol_rate(u32 baseaddr,int value,u32 *read_value);
int receive_axi_modulation_select(u32 baseaddr,int value,u32 *read_value);
int receive_axi_scrambler_enable(u32 baseaddr,int value,u32 *read_value);
int receive_fec_select(u32 baseaddr,int value,u32 *read_value);
int receive_control_byte_0(u32 baseaddr,u32 *read_value);
int receive_control_byte_1(u32 baseaddr,u32 *read_value);
int receive_control_byte_2(u32 baseaddr,u32 *read_value);
int receive_control_byte_3(u32 baseaddr,u32 *read_value);
int receive_control_byte_4(u32 baseaddr,u32 *read_value);
int receive_control_byte_5(u32 baseaddr,u32 *read_value);
int receive_control_byte_N(u32 baseaddr,u8 num_control_bytes);
int receive_dummy_data_U32(u32 baseaddr,int value,u32 *read_value);
int receive_axi_idle_mode_enable(u32 baseaddr,int value,u32 *read_value);
int receive_axi_version_register(u32 baseaddr,int value,u32 *read_value);
int receive_axi_changelist(u32 baseaddr,int value,u32 *read_value);

// DDS AXI4-Lite interface control drivers
int dds_axi_set_power_down(u32 baseaddr, u32 value, u32 *read_value);
int dds_axi_read_pll_lock_status(u32 baseaddr,u32 *read_value);
int dds_axi_dac_calibration_register(u32 baseaddr,u32 value,u32 *read_value);
int dds_axi_frequency_list_depth(u32 baseaddr, u8 value, u32 *read_value);
int dds_axi_set_hop_clock_trigger_edge(u32 baseaddr, u32 value, u32 *read_value);
int dds_axi_set_start_stop_hop(u32 baseaddr, u32 value, u32 *read_value);
int dds_axi_set_single_frequency_mode(u32 baseaddr, u32 value, u32 *read_value);
int dds_axi_set_tx_program_value(u32 baseaddr, u32 value, u32 *read_value);
int dds_axi_set_rx_program_value(u32 baseaddr, u32 value, u32 *read_value);
int dds_axi_hop_table_address_reset(u32 baseaddr, u32 value, u32 *read_value);
int dds_axi_write_custom_register(u32 baseaddr, u32 value, u8 address);
int dds_axi_set_hopping_rate(u32 baseaddr, u32 value, u32 *read_value);
int dds_axi_read_readback_register_1(u32 baseaddr,u32 *read_value);
int dds_axi_read_readback_register_2(u32 baseaddr,u32 *read_value);
int dds_axi_read_readback_register_3(u32 baseaddr,u32 *read_value);
// Memory interfaces APIs call
int dds_axi_update_hopping_frequency_list(u32 baseaddr,u32 value[],u8 depth);
int *dds_axi_read_register_memory(u32 baseaddr );
int *dds_axi_update_hopping_amplitude_list(u32 baseaddr, u8 depth );
int *dds_axi_read_frequency_table_memory(u32 baseaddr, u8 depth );



