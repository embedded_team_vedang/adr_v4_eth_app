
#define COLLECTING 0
#define SENDING 1

#define WORD_SIZE 4 /* Size of words in bytes */
#define START_FRAME 0x02
#define MAX_ETHERNET_BYTES 1800
#define OVERHEAD_BYTES 2
#define SEND_PACKET

#define PACKET_LENGTH 256
#define PACKET_CHUNK_WORD_SIZE (PACKET_LENGTH >> 2) /*Packet Word Size*/

#define NUM_PACKETS 100000
// Functionality Defines

#define ADD_SOURCE_MAC(x) ((0x090201350A00 >> (x * 8)) & 0xFF) //00 0A 35 01 02 09
#define FORWARD_TO_MAC(x) ((0x48A0D56E2BAC >> (x * 8)) & 0xFF) //AC 2B 6E D5 A0 48

#define MIN_DATA_TO_FORWARD 0
#define MAX_DATA_TO_FORWARD 1500

#define GROUP_TRANSMIT 0x3F

#define TRANSFER_PACKETS 1000
#define TIMER_DIVIDER 1

#define RECEIVER
#define TRANSMITTER
#define MASTER
#define MODEM_CONFIG
// For Enabling different sets of configurations: 00-QPSK2Mbps;01-QAM2Mbps;10-QPSK4Mbps;11-QAM8Mbps
#define MODEM_CONFIG_NUM 0x00

// For prinitng AXI Transaction messages
#define PRINT_AXI_MESSAGE



// Few Example Values can be written to Modem
#define TRANSMIT_SYMBOL_RATE 0x0000						//DEFAULT
#define TRANSMIT_MODULATION_SELECT 0x0000				//DEFAULT
#define TRANSMIT_DELAY_HOP_CLOCK_RESET 0x0001			//DEFAULT
#define TRANSMIT_START_PULSE_WIDTH 0x01EB				//DEFAULT
#define TRANSMIT_START_DELAY 0x016F						//DEFAULT
#define TRANSMIT_IDLE_MODE_EN 0x0001					//DEFAULT
#define TRANMSIT_MESSAGE_MODE 0x0001					//DEFAULT
#define TRANSMIT_CONTINUOUS_MODE 0x0001


#define MAX_FREQUENCY_LIST_DEPTH 256
#define MAX_DDS_REGISTER_READ_ADDRESS 27



