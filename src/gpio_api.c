/**
 * @file 	pl_comms_axi.c
 * @brief 	Contain AXI read write commands
 * @date 	June 18, 2019
 */


#include "gpio_api.h"
#include "xparameters.h"
#include "xil_io.h"
#include "xstatus.h"
#include "stdio.h"
#include "sleep.h"

int gpio_control_system_reset(XGpio *InstancePtr) {
	int Status = XST_SUCCESS;
	const int DeviceId = GPIO_0_EXAMPLE_DEVICE_ID;
	int reset = 0x01;  // Write HIGH in case System Reset is Active HIGH
	const int channel = GPIO_TRI_IO_0_CHANNEL;
	/* Initialize the GPIO driver */
	Status  =  XGpio_Initialize(InstancePtr, DeviceId);
		if (Status != XST_SUCCESS) {
			xil_printf("GPIO_INFO: GPIO Initialization Failed\r\n");
		return XST_FAILURE;
		}
	/* Set the direction for all signals as inputs except the LED output */
	XGpio_SetDataDirection(InstancePtr, channel, 0x00 );
	printf("SYSTEM_INFO:Appylying System Reset for 1 second\n\r");
	XGpio_DiscreteWrite(InstancePtr, channel, (reset<<0));
	sleep(1);
	printf("SYSTEM_INFO:Self-Clear System Reset\n\r");
	XGpio_DiscreteWrite(InstancePtr, channel, (~reset<<0));
  	return XST_SUCCESS;
}

int gpio_control_register_init(XGpio *InstancePtr) {
	int Status = XST_SUCCESS;
	const int DeviceId = GPIO_0_EXAMPLE_DEVICE_ID;
	int init = 0x01;  // Write HIGH in case System Reset is Active HIGH
	const int channel = GPIO_TRI_IO_0_CHANNEL;
	/* Initialize the GPIO driver */
	Status  =  XGpio_Initialize(InstancePtr, DeviceId);
		if (Status != XST_SUCCESS) {
			xil_printf("GPIO_INFO: GPIO Initialization Failed\r\n");
		return XST_FAILURE;
		}
	/* Set the direction for all signals as inputs except the LED output */
	XGpio_SetDataDirection(InstancePtr, channel, 0x00 );
	printf("SYSTEM_INFO: AXI-Lite Register re-initialization\n\r");
	XGpio_DiscreteWrite(InstancePtr, channel, (init<<1));
	sleep(1);
	printf("SYSTEM_INFO:Self-Clear re-initialization bit\n\r");
	XGpio_DiscreteWrite(InstancePtr, channel, (~init<<1));
  	return XST_SUCCESS;
}


int gpio_control_master_slave(XGpio *InstancePtr) {
	int Status = XST_SUCCESS;
	const int DeviceId = GPIO_0_EXAMPLE_DEVICE_ID;
	int master = 0x01;
	const int channel = GPIO_TRI_IO_0_CHANNEL;
	/* Initialize the GPIO driver */
	Status  =  XGpio_Initialize(InstancePtr, DeviceId);
		if (Status != XST_SUCCESS) {
			xil_printf("GPIO_INFO: GPIO Initialization Failed\r\n");
		return XST_FAILURE;
		}
	/* Set the direction for all signals as inputs except the LED output */
	XGpio_SetDataDirection(InstancePtr, channel, 0x00000000 );
	printf("SYSTEM_INFO:Configuring System as MASTER Mode\n\r");
	XGpio_DiscreteWrite(InstancePtr, channel, (master<<2));
  	return XST_SUCCESS;
}


int gpio_control_modem_configuration(XGpio *InstancePtr, u16 configuration_id) {
	int Status = XST_SUCCESS;
	const int DeviceId = GPIO_0_EXAMPLE_DEVICE_ID;
	int config = configuration_id;
	const int channel = GPIO_TRI_IO_0_CHANNEL;
	/* Initialize the GPIO driver */
	Status  =  XGpio_Initialize(InstancePtr, DeviceId);
		if (Status != XST_SUCCESS) {
			xil_printf("GPIO_INFO: GPIO Initialization Failed\r\n");
		return XST_FAILURE;
		}
	/* Set the direction for all signals as inputs except the LED output */
	XGpio_SetDataDirection(InstancePtr, channel, 0x00 );
	printf("SYSTEM_INFO:Configuring modem for different configuration\n\r");
	XGpio_DiscreteWrite(InstancePtr, channel, (config<<4));
	sleep(1);
	XGpio_DiscreteWrite(InstancePtr, channel, (config<<4 | 0x01<<3));
	sleep(1);
	XGpio_DiscreteWrite(InstancePtr, channel, (config<<4 | 0x00<<3));
  	return XST_SUCCESS;
}
