/**
 * @file 	pl_comms_axi.c
 * @brief 	Contain AXI read write commands
 * @date 	June 18, 2019
 */


#include "pl_comms_axi.h"
#include "xparameters.h"
#include "xil_types.h"
#include "xil_io.h"
#include "xstatus.h"
#include "stdio.h"
#include "common_header.h"
#include <inttypes.h>
#include "sleep.h"

int transmit_axi_symbol_rate(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_SYMBOL_RATE_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	if (value > 0) {
	if (value==1){
		printf("AXI_INFO:Writing to a Tx register--->Symbol Rate--> 3.072 MHz \n\r");
		printf("AXI_INFO:Done Writing\n\r");
	}
	else {
		printf("AXI_INFO:Writing to a Tx register--->Symbol Rate--> 1.536 MHz \n\r");
		printf("AXI_INFO:Done Writing\n\r");
	}
	}
	else {
		printf("AXI_INFO:Skip Writing Tx register--->Symbol Rate \n\r");
	}
	#endif
	if (value > 0) {
		Xil_Out32(baseaddr+offset, value);
		read_val = Xil_In32(baseaddr+offset);
		*read_value = read_val;
		if (read_val!=value){
			return XST_FAILURE;
		}
	}
	else {
		read_val = Xil_In32(baseaddr+offset);
		*read_value = read_val;
	}
	return XST_SUCCESS;
}

int transmit_axi_modulation_select(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_MODULATION_SELECT_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	if (value > 0) {
	if (value==1){
		printf("AXI_INFO:Writing to a Tx register--->Modulation Select--> QAM \n\r");
		printf("AXI_INFO:Done Writing\n\r");
	}
	else {
		printf("AXI_INFO:Writing to a Tx register--->Modulation Select--> QPSK \n\r");
		printf("AXI_INFO:Done Writing\n\r");
	}
	}
	else {
		printf("AXI_INFO:Skip Writing Tx register--->Modulation Select \n\r");
	}
	#endif
	if (value > 0) {
		Xil_Out32(baseaddr+offset, value);
		read_val = Xil_In32(baseaddr+offset);
		*read_value = read_val;
		if (read_val!=value){
			return XST_FAILURE;
		}
	}
	else {
		read_val = Xil_In32(baseaddr+offset);
		*read_value = read_val;
	}
	return XST_SUCCESS;
}

int transmit_axi_delay_hop_clock_reset(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_DELAY_HCLK_RESET_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Tx register--->Transmit Delay Hop-Clock Reset\n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_axi_start_pulse_width(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_START_PULSE_WIDTH_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Tx register--->Transmit Start Pulse Width \n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_axi_start_delay(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_START_DELAY_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
    printf("AXI_INFO:Writing to a Tx register--->Transmit Start Pulse Delay\n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_axi_message_type(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_MESSAGE_TYPE_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	if (value==1){
		printf("AXI_INFO:Writing to a Tx register--->Message Type--> External\n\r");
	}
	else{
		printf("AXI_INFO:Writing to a Tx register--->Message Type--> Internal\n\r");
	}
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_axi_transmit_request(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_REQUEST_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	if (value==1){
		printf("AXI_INFO:Writing to a Tx register--->Transmit Request--> Enable\n\r");
	}
	else{
		printf("AXI_INFO:Writing to a Tx register--->Transmit Request--> Disable\n\r");
	}
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_axi_transmit_continuous(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_CONTINUOUS_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	if (value==1){
		printf("AXI_INFO:Writing to a Tx register--->Transmit Continuous--> Enable\n\r");
	}
	else{
		printf("AXI_INFO:Writing to a Tx register--->Transmit Continuous--> Disable\n\r");
	}
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}


int transmit_axi_reset_frame_counter(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_FRAME_COUNTER_RESET_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	if (value==1){
		printf("AXI_INFO:Writing to a Tx register--->Reset Frame Counter--> True\n\r");
	}
	else{
		printf("AXI_INFO:Writing to a Tx register--->Reset Frame Counter--> False\n\r");
	}
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_axi_packet_type(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_PACKET_TYPE_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Tx register--->Transmit Packet Type\n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_axi_set_num_packets(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_SET_NUM_PACKETS_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Tx register--->Transmit Num Packets\n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_axi_frame_counter(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_FRAME_COUNT_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	#endif
	//Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_axi_scrambler_enable(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_SCRAMBLER_ENABLE_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	if (value==1){
		printf("AXI_INFO:Writing to a Tx register--->Scrambler --> Enable\n\r");
	}
	else{
		printf("AXI_INFO:Writing to a Tx register--->Scrambler --> Disable\n\r");
	}
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_fec_select(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_FEC_SELECT_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Tx register--->FEC Rate\n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_control_byte_0(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_CONTROL_BYTE_0_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Tx register--->Control Byte-0\n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_control_byte_1(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_CONTROL_BYTE_1_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Tx register--->Control Byte-1\n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_control_byte_2(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_CONTROL_BYTE_2_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Tx register--->Control Byte-2\n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_control_byte_3(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_CONTROL_BYTE_3_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Tx register--->Control Byte-3\n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_control_byte_4(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_CONTROL_BYTE_4_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Tx register--->Control Byte-4\n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_control_byte_5(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_CONTROL_BYTE_5_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Tx register--->Control Byte-5\n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_control_byte_N(u32 baseaddr,u32 value[],u8 num_control_bytes) {
	u32 read_val=0;
	u8 i=0;
	u32 offset = TRANSMIT_CONTROL_BYTE_0_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Tx register--->Control Byte-All\n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	for (i=0;i<=num_control_bytes;i++)
	{
		Xil_Out32(baseaddr+(offset+(i<<2)), value[i]);
		read_val = Xil_In32(baseaddr+(offset+(i<<2)));
		//*read_value = read_val;
		if (read_val!=value[i]){
		return XST_FAILURE;
		}
	}
	return XST_SUCCESS;
}

int transmit_dummy_data_U32(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_DUMMY_DATA_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Tx register--->Dummy Data(32-Bits) \n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_axi_idle_mode_enable(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_IDLE_MODE_ENABLE_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	if (value==1){
		printf("AXI_INFO:Writing to a Tx register--->IDLE Mode--> Enable\n\r");
	}
	else{
		printf("AXI_INFO:Writing to a Tx register--->IDLE Mode--> Disable\n\r");
	}
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}


int transmit_axi_version_register(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_VERSION_REGISTER_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	#endif
	//Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int transmit_axi_changelist(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = TRANSMIT_CHANGELIST_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	#endif
	//Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}


int receive_axi_symbol_rate(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_SYMBOL_RATE_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	if (value > 0) {
	if (value==1){
		printf("AXI_INFO:Writing to a Rx register--->Symbol Rate--> 3.072 MHz \n\r");
		printf("AXI_INFO:Done Writing\n\r");
	}
	else {
		printf("AXI_INFO:Writing to a Rx register--->Symbol Rate--> 1.536 MHz \n\r");
		printf("AXI_INFO:Done Writing\n\r");
	}
	}
	else {
		printf("AXI_INFO:Skip Writing Rx register--->Symbol Rate \n\r");
	}
	#endif
	if (value > 0) {
		Xil_Out32(baseaddr+offset, value);
		read_val = Xil_In32(baseaddr+offset);
		*read_value = read_val;
		if (read_val!=value){
			return XST_FAILURE;
		}
	}
	else {
		read_val = Xil_In32(baseaddr+offset);
		*read_value = read_val;
	}
	return XST_SUCCESS;
}


int receive_axi_modulation_select(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_SYMBOL_RATE_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	if (value > 0) {
	if (value==1){
		printf("AXI_INFO:Writing to a Rx register--->Modulation Select--> QAM \n\r");
		printf("AXI_INFO:Done Writing\n\r");
	}
	else {
		printf("AXI_INFO:Writing to a Rx register--->Modulation Select--> QPSK \n\r");
		printf("AXI_INFO:Done Writing\n\r");
	}
	}
	else {
		printf("AXI_INFO:Skip Writing Rx register--->Modulation Select \n\r");
	}
	#endif
	if (value > 0) {
		Xil_Out32(baseaddr+offset, value);
		read_val = Xil_In32(baseaddr+offset);
		*read_value = read_val;
		if (read_val!=value){
			return XST_FAILURE;
		}
	}
	else {
		read_val = Xil_In32(baseaddr+offset);
		*read_value = read_val;
	}
	return XST_SUCCESS;
}



int receive_axi_scrambler_enable(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_SCRAMBLER_ENABLE_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	if (value==1){
		printf("AXI_INFO:Writing to a Rx register--->Scrambler --> Enable\n\r");
	}
	else{
		printf("AXI_INFO:Writing to a Rx register--->Scrambler --> Disable\n\r");
	}
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int receive_fec_select(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_FEC_SELECT_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Rx register--->FEC Rate\n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int receive_control_byte_0(u32 baseaddr,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_CONTROL_BYTE_0_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Reading a Rx register--->Control Byte-0\n\r");
	#endif
	read_val = Xil_In32(baseaddr+offset);
	#ifdef PRINT_AXI_MESSAGE
	*read_value = read_val;
	#endif
	return XST_SUCCESS;
}

int receive_control_byte_1(u32 baseaddr,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_CONTROL_BYTE_1_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Reading a Rx register--->Control Byte-1\n\r");
	#endif
	read_val = Xil_In32(baseaddr+offset);
	#ifdef PRINT_AXI_MESSAGE
	*read_value = read_val;
	#endif
	return XST_SUCCESS;
}

int receive_control_byte_2(u32 baseaddr,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_CONTROL_BYTE_2_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Reading a Rx register--->Control Byte-2\n\r");
	#endif
	read_val = Xil_In32(baseaddr+offset);
	#ifdef PRINT_AXI_MESSAGE
	*read_value = read_val;
	#endif
	return XST_SUCCESS;
}

int receive_control_byte_3(u32 baseaddr,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_CONTROL_BYTE_3_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Reading a Rx register--->Control Byte-3\n\r");
	#endif
	read_val = Xil_In32(baseaddr+offset);
	#ifdef PRINT_AXI_MESSAGE
	*read_value = read_val;
	#endif
	return XST_SUCCESS;
}

int receive_control_byte_4(u32 baseaddr,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_CONTROL_BYTE_4_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Reading a Rx register--->Control Byte-4\n\r");
	#endif
	read_val = Xil_In32(baseaddr+offset);
	#ifdef PRINT_AXI_MESSAGE
	*read_value = read_val;
	#endif
	return XST_SUCCESS;
}

int receive_control_byte_5(u32 baseaddr,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_CONTROL_BYTE_5_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Reading a Rx register--->Control Byte-5\n\r");
	#endif
	read_val = Xil_In32(baseaddr+offset);
	#ifdef PRINT_AXI_MESSAGE
	*read_value = read_val;
	#endif
	return XST_SUCCESS;
}

int receive_control_byte_N(u32 baseaddr,u8 num_control_bytes) {
	u32 read_val[num_control_bytes+1];
	u8 i=0;
	u32 offset = RECEIVE_CONTROL_BYTE_0_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Reading a Rx register--->Control Byte-All\n\r");
	#endif
	for (i=0;i<=num_control_bytes;i++)
	{
		read_val[i] = Xil_In32(baseaddr+(offset+(i<<2)));
	}
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Done Reading\n\r");
	#endif
	return *read_val;
}


int receive_dummy_data_U32(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_DUMMY_DATA_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a Rx register--->Dummy Data(32-Bits) \n\r");
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int receive_axi_idle_mode_enable(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_IDLE_MODE_ENABLE_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	if (value==1){
		printf("AXI_INFO:Writing to a Rx register--->IDLE Mode--> Enable\n\r");
	}
	else{
		printf("AXI_INFO:Writing to a Rx register--->IDLE Mode--> Disable\n\r");
	}
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}


int receive_axi_version_register(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_VERSION_REGISTER_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	#endif
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int receive_axi_changelist(u32 baseaddr,int value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = RECEIVE_CHANGELIST_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	#endif
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}


/**************************************************************************************************************************/

// DDS-Register communication APIs

int dds_axi_set_power_down(u32 baseaddr,u32 value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_POWER_DOWN_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-TX register--->PowerDown Register--> %lu \n\r",value);
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}


int dds_axi_read_pll_lock_status(u32 baseaddr,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_PLL_LOCK_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Reading a DDS-Tx register--->DDS internal PLL Lock status\n\r");
	#endif
	read_val = Xil_In32(baseaddr+offset);
	#ifdef PRINT_AXI_MESSAGE
	*read_value = read_val;
	#endif
	return XST_SUCCESS;
}

int dds_axi_dac_calibration_register(u32 baseaddr,u32 value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_DAC_CALIBRATION_ENABLE_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-TX register--->DAC Calibration Register--> %lu \n\r",value);
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int dds_axi_frequency_list_depth(u32 baseaddr,u8 value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_FREQUENCY_LIST_DEPTH_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-TX register--->Frequency List Depth--> %u \n\r",value);
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}


int dds_axi_set_hop_clock_trigger_edge(u32 baseaddr,u32 value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_HCLK_TRIGGER_EDGE_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-TX register--->Hop Clock Trigger Edge--> %lu \n\r",value);
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int dds_axi_set_start_stop_hop(u32 baseaddr,u32 value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_HOP_START_STOP_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-TX register--->Is Hop Start?--> %lu \n\r",value);
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}



int dds_axi_set_single_frequency_mode(u32 baseaddr,u32 value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_SINGLE_FREQ_MODE_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-TX register--->Set Single Frequency Mode--> %lu \n\r",value);
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int dds_axi_set_tx_program_value(u32 baseaddr,u32 value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_TRANSMIT_FREQUENCY_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-TX register--->DDS Tx Frequency Value--> %lu \n\r",value);
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int dds_axi_set_rx_program_value(u32 baseaddr,u32 value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_RECEIVE_FREQUENCY_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-RX register--->DDS Rx Frequency Value--> %lu \n\r",value);
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}


int dds_axi_hop_table_address_reset(u32 baseaddr,u32 value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_HOP_TABLE_READ_ADDRESS_RESET_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-TX register--->Hop Table Reset Value--> %lu \n\r",value);
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int dds_axi_write_custom_register(u32 baseaddr, u32 value, u8 address) {
	u32 read_val=0;
	u32 offset = DDS_CUSTOM_ADDRESS_OFFSET;
#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-TX register--->DDS Custom Address--> %lu \n\r",value);
	printf("AXI_INFO:Done Writing\n\r");
#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	if (read_val!=value){
		return XST_FAILURE;
	}
    usleep(10);
	read_val=0;
	offset = DDS_CUSTOM_DATA_OFFSET;
#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-TX register--->DDS Custom Data--> %d \n\r",address);
	printf("AXI_INFO:Done Writing\n\r");
#endif
	Xil_Out32(baseaddr+offset, address);
	read_val = Xil_In32(baseaddr+offset);

	if (read_val!=address){
		return XST_FAILURE;
	}

	usleep(10);
	read_val=0;
	u32 write_enable = 1;
	offset = DDS_CUSTOM_WRITE_ENABLE_OFFSET;
#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-TX register--->DDS Custom Data Write Enable--> %lu \n\r",write_enable);
	printf("AXI_INFO:Done Writing\n\r");
#endif
	Xil_Out32(baseaddr+offset, write_enable);
	read_val = Xil_In32(baseaddr+offset);
	if (read_val!=write_enable){
		return XST_FAILURE;
	}

	usleep(10);
	write_enable = 0;
#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-TX register--->DDS Custom Data Write Enable--> %lu \n\r",write_enable);
	printf("AXI_INFO:Done Writing\n\r");
#endif
	Xil_Out32(baseaddr+offset, write_enable);
	read_val = Xil_In32(baseaddr+offset);
	if (read_val!=write_enable){
		return XST_FAILURE;
	}

	return XST_SUCCESS;

}

int dds_axi_set_hopping_rate(u32 baseaddr,u32 value,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_HOP_RATE_OFFSET;
    #ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Writing to a DDS-TX register--->DDS Hopping Rate--> %lu \n\r",value);
	printf("AXI_INFO:Done Writing\n\r");
	#endif
	Xil_Out32(baseaddr+offset, value);
	read_val = Xil_In32(baseaddr+offset);
	*read_value = read_val;
	if (read_val!=value){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}






int dds_axi_read_readback_register_1(u32 baseaddr,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_READBACK_REGISTER_1_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Reading a DDS-Tx register--->DDS Readback Register-1\n\r");
	#endif
	read_val = Xil_In32(baseaddr+offset);
	#ifdef PRINT_AXI_MESSAGE
	*read_value = read_val;
	#endif
	return XST_SUCCESS;
}



int dds_axi_read_readback_register_2(u32 baseaddr,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_READBACK_REGISTER_2_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Reading a DDS-Tx register--->DDS Readback Register-2\n\r");
	#endif
	read_val = Xil_In32(baseaddr+offset);
	#ifdef PRINT_AXI_MESSAGE
	*read_value = read_val;
	#endif
	return XST_SUCCESS;
}




int dds_axi_read_readback_register_3(u32 baseaddr,u32 *read_value) {
	u32 read_val=0;
	u32 offset = DDS_READBACK_REGISTER_3_OFFSET;
	#ifdef PRINT_AXI_MESSAGE
	printf("AXI_INFO:Reading a DDS-Tx register--->DDS Readback Register-3\n\r");
	#endif
	read_val = Xil_In32(baseaddr+offset);
	#ifdef PRINT_AXI_MESSAGE
	*read_value = read_val;
	#endif
	return XST_SUCCESS;
}


int dds_axi_update_hopping_frequency_list(u32 baseaddr,u32 value[],u8 depth) {

	u8 i        = 0;
	u32 start_offset = DDS_FREQUENCY_TABLE_START_ADDRESS;
	//u32 end_offset   = DDS_FREQUENCY_TABLE_END_ADDRESS;
	#ifdef PRINT_AXI_MESSAGE
		printf("AXI_INFO:Writing to a DDS-TX Memory--->DDS Hopping Table update\n");

	#endif

	for (i=0;i<depth;i++)
	{
		 printf("AXI_INFO:Done Writing at offset 0x%08X\n\r",(i<<2));
		 Xil_Out32(baseaddr+start_offset+(i<<2), value[i]);
		 usleep(10);
	}
	return XST_SUCCESS;
}



int* dds_axi_read_register_memory(u32 baseaddr ) {

   static int  read_val[MAX_DDS_REGISTER_READ_ADDRESS];
   u8 depth    = 27;
   u32 start_offset = DDS_AD9914_REGISTER_START_ADDRESS;
   //u32 end_offset   = DDS_AD9914_REGISTER_END_ADDRESS;

	#ifdef PRINT_AXI_MESSAGE
		printf("AXI_INFO:Reading to a AD9914 Register Memory--->\n");
		printf("AXI_INFO:Done Reading\n\r");
	#endif

	for (int i=0;i< depth;i++)
	{
	  read_val[i] = Xil_In32(baseaddr+start_offset+(i<<2));
	  usleep(10);
	}
	return  read_val;

}


int* dds_axi_update_hopping_amplitude_list(u32 baseaddr, u8 depth ) {

   static int  read_val[MAX_FREQUENCY_LIST_DEPTH];
   int i;
   u32 start_offset = DDS_AMPLITUDE_TABLE_START_ADDRESS;
   //u32 end_offset   = DDS_AMPLITUDE_TABLE_END_ADDRESS;

	#ifdef PRINT_AXI_MESSAGE
		printf("AXI_INFO:Reading to a AD9914 Amplitude Table Memory--->\n");
		printf("AXI_INFO:Done Reading\n\r");
	#endif

	for (i=0;i< depth;i++)
	{
	  read_val[i] = Xil_In32(baseaddr+start_offset+(i<<2));
	  usleep(10);
	}
	return  read_val;

}




int* dds_axi_read_frequency_table_memory(u32 baseaddr, u8 depth ) {

   static int  read_val[MAX_FREQUENCY_LIST_DEPTH];
   int i;
   u32 start_offset = DDS_FREQUENCY_TABLE_START_ADDRESS;

	#ifdef PRINT_AXI_MESSAGE
		printf("AXI_INFO:Reading to a AD9914 Frequency Table Memory--->\n");
		printf("AXI_INFO:Done Reading\n\r");
	#endif

	for (i=0;i< depth;i++)
	{
	  read_val[i] = Xil_In32(baseaddr+start_offset+(i<<2));
	  usleep(10);
	}
	return  read_val;

}

